from django.conf import settings
from datetime import date
import logging

logging.basicConfig(
    format = '%(asctime)s %(levelname)s: %(message)s',
    filename = settings.LOG_DIR_PATH + '/'\
        + date.today().strftime( settings.LOG_FILE_DATE_FORMAT ) + settings.LOG_FILE_EXTENSION,
    level = logging.DEBUG
)
