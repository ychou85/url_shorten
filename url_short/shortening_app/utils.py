
from shortening_app_logging import logging
import hashlib
import base64
from .models import Short_URL, Target_URL, Device_Type
# Utility functions for views

# global variables
MAX_TARGET_LEN = 2147483647 
MAX_DEVICE_NAME_LEN = 1000
MAX_SHORT_NAME_LEN = 100 
MIN_CUSTOM_ID_LEN = 4
MAX_CUSTOM_ID_LEN = 50

ALL_INPUT_OK = 0
TARGET_NAME_TOO_LONG = 1
SHORT_NAME_TOO_LONG = 2
DEVICE_NAME_TOO_LONG = 3
INVALID_DEVICE_NAME = 4
MISSING_INPUT_PARAMETER = 5
INVALID_METHOD = 6
SERVER_ERROR = 7

ResponseStatusCodes = {'OK' : 1, 'ERROR': 2}
ErrorMessages = { ALL_INPUT_OK : 'Success', 
                  TARGET_NAME_TOO_LONG: 'Max target url length is ' +
                  str(MAX_TARGET_LEN),
                  SHORT_NAME_TOO_LONG: 'Max short url length is ' +
                  str(MAX_SHORT_NAME_LEN),
                  DEVICE_NAME_TOO_LONG: 'Max device name length is ' +
                  str(MAX_DEVICE_NAME_LEN),
                  INVALID_DEVICE_NAME: 'Invalid device name',
                  MISSING_INPUT_PARAMETER: 'One or more parameters are missing',
                  INVALID_METHOD: 'Invalid method used',
                  SERVER_ERROR: 'Server Error',
}


# check input length by type
def check_input_length(input_str, input_type):
    logging.debug('-------------check_input_length')
    if input_type == "target":
        if len(input_str) > MAX_TARGET_LEN:
            return TARGET_NAME_TOO_LONG
    elif input_type == "short":
        if len(input_str) > MAX_SHORT_NAME_LEN:
            return SHORT_NAME_TOO_LONG
    else:
        if len(input_str) > MAX_DEVICE_NAME_LEN:
            return DEVICE_NAME_TOO_LONG

    return ALL_INPUT_OK




# check for valid device type:
def check_device_type(device_name):
    logging.debug('-------------check_device_type start')
    try:
        device_type = Device_Type.objects.get(name=device_name)
    except:
        return False
    return True





# check for preexisting shortened url:
def check_short_url_exists(target_url, device_type):
    logging.debug('-------------check_short_url_exists start')

    if not check_device_type(device_type):
        raise Exception('Invalid device type')
    check_input = check_input_length(target_url, "target")
    if check_input:
        raise Exception(ErrorMessages.get(check_input))   

    try:
        md5_hash = hashlib.md5()
        md5_hash.update(target_url)
        target_md5 = md5_hash.hexdigest()
        device_id = Device_Type.objects.get(name=device_type)
        existing_short_url = list(Target_URL.objects.filter(md5_hash=target_md5).filter(device_type=device_id))
        if len(existing_short_url):
            return existing_short_url[0].short_url.short_url
        else:
            return None
    except:
        raise





#create custom id for base64 encoding of new short url
# so as to make the urls less predictable:
def create_custom_id(last_short_url_id):
    logging.debug('-------------create_custom_id start')

    custom_id = ""
    # should probably come up with better pseudorandom equation
    last_short_url_id = str(last_short_url_id + 152)
    length = len(last_short_url_id)
    if length < MIN_CUSTOM_ID_LEN:
        custom_id = "0" * (MIN_CUSTOM_ID_LEN - length) + last_short_url_id
    elif length > MAX_CUSTOM_ID_LEN:
        raise Exception('Out of Ids')
    else:
        custom_id = last_short_url_id
    return custom_id





#create a short URL:
def create_short_url(devices_to_targets):
    logging.debug('-------------create_short_url start')

    new_short_url = None
    try:
        last_short_url_id = Short_URL.objects.latest()
    except Exception as e:
        logging.error('-------------create_short_url Exception: ' + str(e))
        last_short_url_id = 1
    try:
        new_custom_id = create_custom_id(last_short_url_id)
        new_short_url = base64.b64encode(new_custom_id, '#!')
        s = Short_URL(custom_id=new_custom_id, short_url=new_short_url)
        s.save()
        for target in devices_to_targets:
            device_name = target.get("device_type")
            target_url = target.get("target_url")
            md5_hash = hashlib.md5()
            md5_hash.update(target_url)
            target_md5 = md5_hash.hexdigest()
            device_id = Device_Type.objects.get(name=device_name)
            t = Target_URL(short_url=s, target_url=target_url, md5_hash=target_md5, device_type=device_id) 
            t.save()
    except Exception as e:
        raise
    finally:
        return new_short_url
    

 


def get_user_agent(ua):
    logging.debug('-------------get_user_agent start')

    device_name = 'Desktop Computer'
    #TODO: check for browser window size to determine laptops 
    if ua.find("iphone") > 0:
        device_name = "iphone"
    elif ua.find("ipad") > 0:
        device_name = "ipad"
    elif ua.find("android") > 0:
        device_name = "android"

    return device_name



#from django
def dict_fetch_all(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
