from django.db import models

# Create your models here.
class Short_URL(models.Model):
    custom_id = models.CharField(max_length=50, unique=True)
    short_url = models.CharField(max_length=100, unique=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'shorturl'

    def __unicode__(self):
        return self.short_url



class Device_Type(models.Model):
    name = models.CharField(max_length=1000)

    class Meta:
        db_table = 'devicetype'

    def __unicode__(self):
        return self.name




class Target_URL(models.Model):
    short_url = models.ForeignKey(Short_URL, to_field="short_url", 
                on_delete=models.CASCADE)
    target_url = models.CharField(max_length=2147483647)
    md5_hash = models.CharField(max_length=16)
    device_type = models.ForeignKey(Device_Type, on_delete=models.PROTECT)
    redirect_count = models.IntegerField(default=0)

    class Meta:
        db_table = 'targeturl'
        index_together = ["device_type", "md5_hash"]

    def __unicode__(self):
        return self.target_url




