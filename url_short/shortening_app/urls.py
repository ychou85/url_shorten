from django.conf.urls import url

from . import views

#note that the way these are ordered, if we shorten a target
#to "shorten" or "list_all" we will run into trouble
#but I think the right solution for that is to deploy
#the api on a separate subdomain rather than catch this
#in the shortening logic
urlpatterns = [
    url(r'^shorten/$', views.shorten, name='shorten'),
    url(r'^list_all/$', views.list_all, name='list_all'),
    url(r'^(?P<short_url>[^/]+)/$', views.direct_them, name='direct_them'),
]
