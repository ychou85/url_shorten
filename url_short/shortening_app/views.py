from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from shortening_app_logging import logging
from utils import *
import json, datetime
from django.db import connection
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def direct_them(request, short_url):
    logging.debug('-------------direct them start')
    response_to_frontend = {}
    response_to_frontend['code'] = ResponseStatusCodes['OK']
    response_to_frontend['message'] = 'OK'

    if request.method != "GET":
        response_to_frontend['code'] = ResponseStatusCodes['ERROR']
        response_to_frontend['message'] = ErrorMessages[INVALID_METHOD]
        return HttpResponse(
                    json.dumps(response_to_frontend),
                    status=200,
                    content_type='application/json'
                )

    device_name = get_user_agent(request.META.get('HTTP_USER_AGENT', '').lower())

    try:
        s = Short_URL.objects.get(short_url=short_url)
        d = Device_Type.objects.get(name=device_name)
        t = Target_URL.objects.filter(short_url=s).filter(device_type=d)
        t[0].redirect_count = t[0].redirect_count + 1
        t[0].save()
        link = t[0].target_url
        if "http" not in link:
            link = "http://" + link 
        return HttpResponseRedirect(link)

    except Exception as e:
        logging.error(str(e))
        response_to_frontend['code'] = ResponseStatusCodes['ERROR']
        response_to_frontend['message'] = ErrorMessages[SERVER_ERROR]
        return HttpResponse(
                    json.dumps(response_to_frontend),
                    status=200,
                    content_type='application/json'
                )
        




@csrf_exempt
def shorten(request):
    logging.debug('-------------shorten start')
    response_to_frontend = {}
    response_to_frontend['code'] = ResponseStatusCodes['OK']
    response_to_frontend['message'] = 'OK'

    if request.method != "POST":
        response_to_frontend['code'] = ResponseStatusCodes['ERROR']
        response_to_frontend['message'] = ErrorMessages[INVALID_METHOD]
        return HttpResponse(
                    json.dumps(response_to_frontend),
                    status=200,
                    content_type='application/json'
                )

    payload = request.POST if request.POST else request.body
    if payload:
        payload = json.loads(payload)
        devices_to_targets = payload.get('devices_to_targets')
        short_url_set = set([])

        try:
            for target in devices_to_targets:
                short_url = check_short_url_exists(target.get("target_url"), target.get("device_type"))
                if short_url:
                    short_url_set.add(short_url)

            # if all short urls are the same for all requested devices
            # then we can reuse it
            if len(short_url_set) == 1:
                response_to_frontend['short_url'] = short_url_set.pop()
            else:
                new_short_url = create_short_url(devices_to_targets)
                if new_short_url:
                    response_to_frontend['short_url'] = new_short_url
        except Exception as e:
            logging.error('shorten: Exception ' + str(e))
        

    return HttpResponse(
                json.dumps(response_to_frontend),
                status=200,
                content_type='application/json'
            )



 

@csrf_exempt
def list_all(request):
    logging.debug('-------------list_all start')
    response_to_frontend = {}
    response_to_frontend['code'] = ResponseStatusCodes['OK']
    response_to_frontend['message'] = 'OK'

    if request.method != "GET":
        response_to_frontend['code'] = ResponseStatusCodes['ERROR']
        response_to_frontend['message'] = ErrorMessages[INVALID_METHOD]
        return HttpResponse(
                    json.dumps(response_to_frontend),
                    status=200,
                    content_type='application/json'
                )
    short_url_response_list = []
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT s.short_url, s.created_date, t.target_url," +
                        "d.name as device_type, t.redirect_count FROM shorturl s " +
                        "INNER JOIN targeturl t ON s.short_url = t.short_url_id INNER JOIN " +
                        "devicetype d ON d.id = t.device_type_id ORDER BY s.created_date")
        results = dict_fetch_all(cursor)
        for short_url_obj in results:
             #currently returns in UTC, should ideally localize?
            short_url_obj['created_date'] = short_url_obj.get('created_date').strftime('%m/%d/%Y %I:%M %p')
            short_url_response_list.append(short_url_obj)

        response_to_frontend['results'] = short_url_response_list

    except Exception as e:
        logging.error(str(e))
        response_to_frontend['code'] = ResponseStatusCodes['ERROR']
        response_to_frontend['message'] = ErrorMessages[SERVER_ERROR]
    finally:
        return HttpResponse(
                    json.dumps(response_to_frontend),
                    status=200,
                    content_type='application/json'
                )
