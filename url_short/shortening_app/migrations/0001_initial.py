# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Device_Type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=1000)),
            ],
            options={
                'db_table': 'devicetype',
            },
        ),
        migrations.CreateModel(
            name='Short_URL',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('custom_id', models.CharField(unique=True, max_length=50)),
                ('short_url', models.CharField(unique=True, max_length=100)),
                ('md5_hash', models.CharField(unique=True, max_length=16)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'db_table': 'shorturl',
            },
        ),
        migrations.CreateModel(
            name='Target_URL',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('target_url', models.CharField(max_length=2147483647)),
                ('redirect_count', models.IntegerField(default=0)),
                ('device_type', models.ForeignKey(to='shortening_app.Device_Type', on_delete=django.db.models.deletion.PROTECT)),
                ('short_url', models.ForeignKey(to='shortening_app.Short_URL', to_field=b'short_url')),
            ],
            options={
                'db_table': 'targeturl',
            },
        ),
    ]
