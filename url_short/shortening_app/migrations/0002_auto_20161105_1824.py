# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shortening_app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='short_url',
            name='md5_hash',
        ),
        migrations.AddField(
            model_name='target_url',
            name='md5_hash',
            field=models.CharField(default=None, max_length=16),
            preserve_default=False,
        ),
        migrations.AlterIndexTogether(
            name='target_url',
            index_together=set([('device_type', 'md5_hash')]),
        ),
    ]
